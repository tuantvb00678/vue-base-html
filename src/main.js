import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router";
import VueResource from 'vue-resource'

//Debug
Vue.config.debug = true;

Vue.use(VueRouter);
Vue.use(VueResource);

import Login from './components/login/login.vue'
import Signup from './components/signup/signup.vue'
import Dashboard from './components/dashboard/dashboard.vue'
// Ngành hàng
import Industry from './components/industry/industry.vue'
// Danh sách sản phẩm
import Collection from './components/collection/collection.vue'
import Product from './components/product/product.vue'
import Order from './components/order/order.vue'
import Shipping from './components/shipping/shipping.vue'
import OrderDetails from './components/order-details/order-details.vue'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      component: Dashboard
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/signup',
      component: Signup
    },
    {
      path: '/nganh-hang',
      component: Industry
    },
    {
      path: '/collection',
      component: Collection
    },
    {
      path: '/product',
      component: Product
    },
    {
      path: '/order',
      component: Order
    },
    {
      path: '/order-details',
      component: OrderDetails
    },
    {
      path: '/shipping',
      component: Shipping
    }
  ]
})


const app = new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')
